
namespace Malquarium {

    using System;
    using System.Diagnostics;
    using System.Threading;

    using Malquarium.Sltp;

    public class TestGameManager {


        public static void Main(string[] args) {

            // initial setup
            var gameManager = new TestGameManager();
            gameManager.Start();

            // main game loop
            while (true) {
                // Wait 1 second to simulate 1 FPS ;-)
                Thread.Sleep(1000);
                gameManager.Update();
            }
        }

        /* =======================================
         *
         *       FAKE GAME MANAGER EXAMPLE
         *                BELOW
         *
         *                  |
         *                  V
         *
         * ======================================= */

        private TextureServer server;

        // This is meant as the MonoBehavior.Start() method.
        public void Start() {
            var settings = new TextureServerSettings {
                UseFileBackup = true,
                BackupFolderPath = "./received",
                BufferType = TextureBufferType.Stack
            };
            this.server = new TextureServer(settings);
            server.Start();
        }

        public void Update() {
            Console.WriteLine("Next frame!");
            if(server.TryGetNext(out var data)) {
                Console.WriteLine($"Found on input queue: {data}");
                // todo: Create new texture and spawn new fish.
            }
        }
    }
}

