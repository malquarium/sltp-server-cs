
CSC=csc
CFLAGS=

SRCFILES =  $(wildcard src/*.cs)
TESTFILES = $(wildcard tests/*.cs)
EXAMPLES =  $(wildcard example/*.cs)


TARGET = build/LibTextureServer.dll

OUTDIR = build
TESTLIB = $(OUTDIR)/MiniTest.dll
EXAMPLE_EXE = $(OUTDIR)/TestGameManager.exe
TESTS_EXE = $(OUTDIR)/UnitTests.exe

TESTLIB_ORIG = lib/MiniTest/MiniTest.dll

all : $(TARGET) $(EXAMPLE_EXE) $(TESTS_EXE)

tests : $(OUTDIR)/UnitTests.exe
	mono $<

clean:
	test -d $(OUTDIR) && rm -rf $(OUTDIR)
	rm *.dll *.exe

$(OUTDIR):
	mkdir $(OUTDIR)

$(TARGET) : $(SRCFILES) $(OUTDIR)
	$(CSC) $(CFLAGS) -doc:$(basename $(TARGET)).xml -target:library -out:$@ $(SRCFILES)

$(TESTLIB) : $(TESTLIB_ORIG) $(OUTDIR)
	cp $< $@

$(EXAMPLE_EXE) : $(EXAMPLES) $(TARGET)
	$(CSC) $(CFLAGS) -r:$(TARGET) -out:$@ $(EXAMPLES)

$(TESTS_EXE) : $(TESTFILES) $(TESTLIB) $(TARGET)
	$(CSC) $(CFLAGS) -r:$(TESTLIB) -r:$(TARGET) -out:$@ $(TESTFILES)

$(TESTLIB_ORIG) :
	$(MAKE) -C lib/MiniTest

.PHONY : all clean

