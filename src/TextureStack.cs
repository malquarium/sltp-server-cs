
namespace Malquarium.Sltp {

    using System.Collections.Concurrent;

    internal class TextureStack
        : ConcurrentStack<FishTextureData>, ITextureBuffer
    {
        /// <inheritdoc/>
        void ITextureBuffer.Push(FishTextureData data) => this.Push(data);

        /// <inheritdoc/>
        bool ITextureBuffer.TryPopNext(out FishTextureData data) =>
            this.TryPop(out data);
    }
}

