
namespace Malquarium.Sltp {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     Implementation of the Sealife transfer protocol (SLTP) V1.
    /// </summary>
    public class SltpV1 {

        private readonly Action<TraceLevel, string> log;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SltpV1" /> class.
        /// </summary>
        public SltpV1(Action<TraceLevel, string> loggerAction) {
            this.log = loggerAction
                ?? throw new ArgumentNullException(nameof(loggerAction));
        }

        /// <summary>
        ///     Asynchronously tries to read a <see cref="FishTextureData" />
        ///     instance from the <paramref name="stream" />
        /// </summary>
        /// <param name="stream">
        ///     The stream to read the serialized fish from.
        /// </param>
        /// <param name="cancellationToken">
        ///     A token to cancel the operation.
        /// </param>
        /// <returns>
        ///     A task representing the async operation.
        ///     The task's result is the fish data, if successful.
        ///     If not successful, <see langword="null" /> is returned.
        /// </returns>
        public async Task<FishTextureData> TryReadFishFromStreamAsync(
            Stream stream,
            CancellationToken cancellationToken) {

            var buf8 = new byte[8];
            int read;

            read = await stream.ReadAsync(buf8, 0, 8, cancellationToken);
            if (read != 8) {
                this.log(TraceLevel.Error, "Invalid PHEAD. Abort.");
                return null;
            }

            if (buf8[0] != 1
                    || buf8[1] != 0
                    || buf8[2] != 0
                    || buf8[3] != 0) {
                this.log(
                    TraceLevel.Error,
                    "Unknown protocol version or malformed PHEAD. Abort.");
                return null;
            }

            var dataLength = BitConverter.ToInt32(buf8, 4);

            read = await stream.ReadAsync(buf8, 0, 8, cancellationToken);
            if (read != 8) {
                this.log(TraceLevel.Error, "Invalid DHEAD. Abort.");
                return null;
            }

            if (buf8[1] != 0
                    || buf8[2] != 0
                    || buf8[3] != 0) {
                this.log(
                    TraceLevel.Error,
                    "Malformed DHEAD. Abort.");
                return null;
            }

            var fishType = (FishType)buf8[0];
            var width = (int)BitConverter.ToInt16(buf8, 4);
            var height = (int)BitConverter.ToInt16(buf8, 6);

            if (width * height * 4L != dataLength) {
                this.log(
                    TraceLevel.Error,
                    "The dimensions if the image do not match the"
                    + " data length");
                return null;
            }

            var pixels = new List<byte>();
            var dataBuf = new byte[1024];
            while((read = await stream.ReadAsync(dataBuf, 0, dataBuf.Length)) > 0) {
                pixels.AddRange(dataBuf.Take(read));
            }

            if (pixels.Count != dataLength) {
                this.log(
                    TraceLevel.Error,
                    "DATA length mismatch. "
                    + $"Received: {pixels.Count}. "
                    + $"Expected: {dataLength}.");
                return null;
            }

            return new FishTextureData(
                fishType,
                width,
                height,
                pixels.ToArray());
        }
    }
}
