
namespace Malquarium.Sltp {

    /// <summary>
    ///     Concurrent input buffer to store received
    ///     <see cref="FishTextureData"/>.
    /// </summary>
    internal interface ITextureBuffer {

        /// <summary>
        ///     Adds a new <see cref="FishTextureData"/> object to the
        ///     buffer.
        /// </summary>
        public void Push(FishTextureData data);

        /// <summary>
        ///     Pops the next <see cref="FishTextureData"/> object
        ///     from the buffer.
        /// </summary>
        public bool TryPopNext(out FishTextureData data);
    }
}
