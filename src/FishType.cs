
namespace Malquarium.Sltp {

    /// <summary>
    ///     Defines the possible fish types.
    /// </summary>
    public enum FishType {

        // "normal" fish types.

        /// <summary>Fish type 1</summary>
        Fish1 = 1,
        /// <summary>Fish type 2</summary>
        Fish2 = 2,
        /// <summary>Fish type 3</summary>
        Fish3 = 3,
        /// <summary>Fish type 4</summary>
        Fish4 = 4,

        /// <summary>
        ///     This fish is meant for special purposes, like
        ///     a stencil found during a kid's treasure hunt.
        /// </summary>
        King = 99
    }
}
