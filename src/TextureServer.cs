
namespace Malquarium.Sltp {

    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     The texture server listens on the given TCP port for new texture
    ///     data messages from the scanner host.
    /// </summary>
    public class TextureServer {

        private const string BackupFileExtension = "fishbak";

        private readonly ITextureBuffer recvBuffer;
        private readonly int tcpPort;
        private readonly Action<TraceLevel, string> log;
        private readonly bool UseBackup;
        private readonly string BackupPath;

        private CancellationTokenSource cts;
        private Task listenTask;

        /// <summary>
        ///     Initializes a new instance of the
        ///     <see cref="TextureServer"/> class.
        /// </summary>
        /// <param name="settings">
        ///     The configuration settings for the server.
        /// </param>
        public TextureServer(TextureServerSettings settings) {
            this.tcpPort = settings.TcpPort;
            this.log = settings.LoggerAction
                ?? throw new ArgumentNullException(
                    "Logger action is not set.");

            this.UseBackup = settings.UseFileBackup;
            this.BackupPath = settings.BackupFolderPath;

            if (this.UseBackup && this.BackupPath == null) {
                throw new ArgumentNullException(
                    "Configured to use backup, but path was null");
            }

            switch (settings.BufferType) {
                case TextureBufferType.Queue:
                    this.recvBuffer = new TextureQueue();
                    break;
                case TextureBufferType.Stack:
                    this.recvBuffer = new TextureStack();
                    break;
                default:
                    throw new ArgumentException(
                        "Invalid buffer type!");
            }
        }

        /// <summary>
        ///     Starts the <see cref="TextureServer"/> and makes it
        ///     listen on the specified port.
        /// </summary>
        public void Start() {
            if(this.listenTask != null) {
                this.log(
                    TraceLevel.Warning,
                    "Start requested, but already started.");
                return;
            }

            this.log(
                TraceLevel.Info,
                $"Starting {nameof(TextureServer)}.");
            this.cts = new CancellationTokenSource();

            Task listenTask;

            if (this.UseBackup) {
               this.log(TraceLevel.Info, "Trying to restore backup");
               listenTask =
                        Task.Run(() => this.Restore())
                        .ContinueWith(_ => this.ListenAsync(this.cts.Token))
                        .Unwrap();
            } else {
                listenTask = this.ListenAsync(this.cts.Token);
            }

            this.listenTask =
                listenTask.ContinueWith(_ => this.listenTask = null);
        }

        /// <summary>
        ///     Stops listening for new texture data.
        /// </summary>
        /// <remarks>
        ///     Due to limitations of the Mono/.NET Framework version,
        ///     the server will not stop waiting for new clients.
        ///     WARNING: This code may be buggy if called multiple times.
        /// </remarks>
        public void Stop() {
            if(this.listenTask == null || this.cts.IsCancellationRequested){
                this.log(TraceLevel.Warning, "Stop requested, but already stopped.");
                return;
            }

            this.cts.Cancel();
            this.log(TraceLevel.Info, "Stopping {nameof(TextureServer)}.");
        }

        /// <summary>
        ///     Tries to get the next <see cref="FishTextureData"/>
        ///     instance from the network queue.
        /// </summary>
        /// <param name="data">
        ///     The dequeued fish data, if data was available.
        ///     <see langword="null"/> otherwise.
        /// </param>
        [Obsolete("Use TryGetNext instead.")]
        public bool TryDequeueNext(out FishTextureData data) =>
            this.TryGetNext(out data);

        /// <summary>
        ///     Tries to get the next <see cref="FishTextureData"/>
        ///     instance from the network buffer.
        /// </summary>
        /// <param name="data">
        ///     The received fish data, if data was available.
        ///     <see langword="null"/> otherwise.
        /// </param>
        public bool TryGetNext(out FishTextureData data) {
            var dequeued = this.recvBuffer.TryPopNext(out data);
            data = dequeued ? data : null;
            return dequeued;
        }

        private async Task ListenAsync(CancellationToken cancellationToken) {
            if (cancellationToken.IsCancellationRequested) {
                return;
            }

            TcpListener server = new TcpListener(IPAddress.Any, this.tcpPort);
            server.Start();

            while (!cancellationToken.IsCancellationRequested) {
                TcpClient client = await server.AcceptTcpClientAsync();
                this.log(TraceLevel.Verbose, "Client connected!");
                NetworkStream stream = client.GetStream();
                var sltp = new SltpV1(this.log);

                var receivedFishData =
                    await sltp.TryReadFishFromStreamAsync(
                        stream,
                        cancellationToken);

                if (receivedFishData == null) {
                    this.log(TraceLevel.Error, "Failed to received fish data");
                    client.Close();
                    continue;
                }

                this.log(
                    TraceLevel.Verbose,
                    $"Received fish data:\n{receivedFishData}");

                if (this.UseBackup) {
                    this.WriteBackup(receivedFishData);
                }

                this.recvBuffer.Push(receivedFishData);

                client.Close();
            }
        }

        private void Restore() {
            if (! Directory.Exists(this.BackupPath)) {
                this.log(
                    TraceLevel.Warning,
                    $"Backup folder not found: {this.BackupPath}");
                return;
            }

            var files = Directory.EnumerateFiles(
                    this.BackupPath,
                    $"*.{BackupFileExtension}").ToList();

            if (files.Count == 0) {
                this.log(TraceLevel.Info, "No backup files found.");
                return;
            }

            this.log(TraceLevel.Info, $"Found {files.Count} backup files");

            foreach (var file in files) {
                var fish = this.LoadBackup(file);
                if (fish == null) {
                    continue;
                }

                this.recvBuffer.Push(fish);
            }
        }

        private FishTextureData LoadBackup(string filePath) {
            if (!File.Exists(filePath)) {
                this.log(TraceLevel.Error, $"File {filePath} does not exist");
                return null;
            }

            try {
                using (var fstream = File.Open(filePath, FileMode.Open)) {
                    return BackupSerialization.ReadFromStream(fstream);
                }
            } catch (Exception ex) {
                this.log(
                    TraceLevel.Error,
                    $"Exception while reading backup file: \n{ex}");
                return null;
            }
        }

        private void WriteBackup(FishTextureData fish) {
            if (! Directory.Exists(this.BackupPath)) {
                this.log(
                    TraceLevel.Info,
                    $"Backup directory created: {this.BackupPath}");

                try {
                    Directory.CreateDirectory(this.BackupPath);
                } catch (Exception ex) {
                    this.log(
                        TraceLevel.Error,
                        $"Exception while creating the backup folder.\n{ex}");
                }
            }

            var fileName =
                DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")
                + $".{BackupFileExtension}";

            this.log(
                TraceLevel.Info,
                $"Writing backup to {fileName}.");

            try {
                using (var fstream = File.Open(
                        Path.Combine(this.BackupPath, fileName),
                        FileMode.Create)) {
                    BackupSerialization.WriteToStream(fstream, fish);
                }
            } catch (Exception ex) {
                this.log(
                    TraceLevel.Error,
                    $"Exception while writing backup file.\n{ex}");
            }
        }
    }
}

