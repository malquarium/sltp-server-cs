
namespace Malquarium.Sltp {

    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    ///     Represents a single fish scan.
    /// </summary>
    public class FishTextureData {

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTextureData"/>
        ///     class.
        /// </summary>
        /// <param name="type">The fish type the texture is for.</param>
        /// <param name="height">The height of the texture in pixels.</param>
        /// <param name="width">The width of the texture in pixels.</param>
        /// <param name="rgbaData">
        ///     The pixel data as sequence of RGBA byte values.
        ///     (4 bytes per pixel.)
        /// </param>
        public FishTextureData(
                FishType type,
                int width,
                int height,
                byte[] rgbaData) {
            this.Type = type;
            this.Height = height;
            this.Width = width;

            if (width * height * 4L != rgbaData.Length) {
                throw new ArgumentException(
                    "RgbaData does not match dimensions.");
            }

            this.RgbaData = Array.AsReadOnly(rgbaData);
        }

        /// <summary>
        ///     Gets the fish type of this DTO.
        /// </summary>
        public FishType Type { get; }

        /// <summary>
        ///     Gets the texture pixel height of this DTO.
        /// </summary>
        public int Height { get; }

        /// <summary>
        ///     Gets the texture pixel width of this DTO.
        /// </summary>
        public int Width { get; }

        /// <summary>
        ///     Gets the texture pixel data as sequence RGBA values.
        ///     4 bytes per pixel, 1 byte per channel.
        /// </summary>
        public ReadOnlyCollection<byte> RgbaData { get; }

        /// <inheritdoc />
        public override string ToString() {
            return
                $"Type: {this.Type}\n" +
                $"H: {this.Height}, W: {this.Width}\n" +
                $"Bytes: {this.RgbaData.Count}\n";
        }
    }
}


