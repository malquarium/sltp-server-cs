# C# SLTP server for Malquarium

The server is provided by the `LibTextureServer.dll`
assembly.

## Build

```sh
make all

cd build/
mono UnitTests.exe
```

## Usage

* Reference the `LibTextureServer.dll` in your C# project.
* Include the `Malquarium.Sltp` namespace.
* Instanciate and start a new `TextureServer`.
* Check for received fish data on every frame.

```csharp
private TextureServer server;

public override void Start() {
    // ...
    var settings = new TextureServerSettings();
    this.server = new TextureServer(settings);
    this.server.Start();
}

//...

public void Update() {
    Console.WriteLine("Next frame!");
    if (server.TryDequeueNext(out var data)) {
        // use data
    }
}
```

See `example/TestGameManager.cs` for a working example
implementation.



